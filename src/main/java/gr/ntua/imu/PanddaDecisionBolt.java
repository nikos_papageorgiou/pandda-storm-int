package gr.ntua.imu;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class PanddaDecisionBolt extends BaseRichBolt {

	OutputCollector _collector;
	private String SERVER_URL;
	private String INSTANCE_UUID;
	private final String SERVICE = "get_action_recommendation";

	public PanddaDecisionBolt(String serverURL, String instanceUUID) {
		this.SERVER_URL = serverURL;
		this.INSTANCE_UUID = instanceUUID;
	}

	@Override
	public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context,
			OutputCollector collector) {
		_collector = collector;
	}

	@Override
	public void execute(Tuple tuple) {
		final String subject = tuple.getString(0);
		//final String lambda = String.valueOf(tuple.getDouble(1));
		String eventId = "1";
		JSONObject obj = callPandda(eventId, subject, tuple.getDouble(1));
		if (obj != null) {
			_collector.emit(tuple,
					new Values(subject, obj.get("action"), obj.get("time")));
			_collector.ack(tuple);
		}
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("subject", "action","time"));
	}

	/*
	 * @param subject : recommendation subject
	 * 
	 * @param predictionLambda : undesired event exp. distribution lambda
	 * (prediction)
	 */
	private JSONObject callPandda(String eventId, String predictionSubject, double predictionLambda) {
		// Creating a new session to a JSON-RPC 2.0 web service at a specified
		// URL

		// The JSON-RPC 2.0 server URL
		URL serverURL = null;

		try {
			serverURL = new URL(this.SERVER_URL);

		} catch (MalformedURLException e) {
			// handle exception...
			e.printStackTrace();
		}

		// Create new JSON-RPC 2.0 client session
		JSONRPC2Session mySession = new JSONRPC2Session(serverURL);

		// Once the client session object is created, you can use to send a
		// series
		// of JSON-RPC 2.0 requests and notifications to it.

		// Construct new request

		// The required named parameters to pass
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("instance", this.INSTANCE_UUID);
		params.put("subject", predictionSubject);
		params.put("prediction_lambda", predictionLambda);
		params.put("event_id", eventId);
		// The mandatory request ID
		String id = "req-001";

		JSONRPC2Request request = new JSONRPC2Request(this.SERVICE, params, id);

		// Send request
		JSONRPC2Response response = null;

		try {
			response = mySession.send(request);

		} catch (JSONRPC2SessionException e) {

			System.err.println(e.getMessage());
			// handle exception...
		}

		// Decode & print response result / error
		if (response.indicatesSuccess()) {
			System.out.println("The request succeeded :");
			System.out.println("\tresult : " + response.getResult());
			System.out.println("\tid     : " + response.getID());
			// Parse response
			// System.out.println(response.toJSONString());
			System.out.println(response.getResult().toString());
			String s = response.getResult().toString();
			if (JSONValue.isValidJson(s))
				System.out.println(s + " validates as Smart JSON");
			try {
				JSONObject obj = (JSONObject) JSONValue.parseStrict(s);
				System.out.println("====== Object content ======");
				System.out.println("instance : " + obj.get("instance"));
				System.out.println("subject : " + obj.get("subject"));
				System.out.println("action : " + obj.get("action"));
				System.out.println("time : " + obj.get("time"));
				return obj;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		} // if not indicatesSuccess
		else {
			System.out.println("The request failed :");
			JSONRPC2Error err = response.getError();
			System.out.println("\terror.code    : " + err.getCode());
			System.out.println("\terror.message : " + err.getMessage());
			System.out.println("\terror.data    : " + err.getData());
			return null;
		}
	}

}