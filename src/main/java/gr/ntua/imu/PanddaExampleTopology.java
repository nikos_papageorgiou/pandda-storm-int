/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package gr.ntua.imu;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.testing.TestWordSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import java.util.Map;
import java.util.Random;

/**
 * This is a basic example of a Storm topology.
 */
public class PanddaExampleTopology {

	@SuppressWarnings("serial")
	public static class SimulatedPredictionBolt extends BaseRichBolt {
		OutputCollector _collector;

		@Override
		public void prepare(Map conf, TopologyContext context,
				OutputCollector collector) {
			_collector = collector;
		}

		@Override
		public void execute(Tuple tuple) {
			final Random rand = new Random();
			final int subjectId = rand.nextInt(1000);
			final double lambda = rand.nextDouble() * 10.0;
			final String subject = tuple.getString(0); 
					//+ "_" + String.valueOf(subjectId) + "_";

			_collector.emit(tuple, new Values(subject, lambda));
			_collector.ack(tuple);
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("subject", "lambda"));
		}

	}
	
	/*
	 * Maven commands
	 * - package : mvn package 
	 * - run 	 : mvn  clean compile exec:java -Dstorm.topology=gr.ntua.imu.PanddaExampleTopology
	 */
	public static void main(String[] args) throws Exception {
		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("event-spout", new SimulatedEventSpout(), 10);
		builder.setBolt("predict", new SimulatedPredictionBolt(), 3)
				.shuffleGrouping("event-spout");
		// builder.setBolt("decide", new TestPanddaDecisionBolt(),
		// 2).shuffleGrouping("predict");
		String instanceUUID = "2aqV5gFq1RuG70OBR_UTb6Q";
		builder.setBolt(
				"decide",
				new PanddaDecisionBolt(
						"http://snf-542682.vm.okeanos.grnet.gr/pandda/services/call/jsonrpc2",
						instanceUUID), 2).shuffleGrouping("predict");

		Config conf = new Config();
		conf.setDebug(true);

		if (args != null && args.length > 0) {
			conf.setNumWorkers(3);

			StormSubmitter.submitTopologyWithProgressBar(args[0], conf,
					builder.createTopology());
		} else {

			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology("test", conf, builder.createTopology());
			Utils.sleep(5 * 1000);
			cluster.killTopology("test");
			cluster.shutdown();
		}
	}
}
